use crate::utils::read_lines;
use regex::Regex;
use std::collections::{HashMap, HashSet};


pub fn resolve_binary(code: &str, mut min: u32, mut max: u32) -> u32 {
	for c in code.chars() {
		if c == 'F' || c == 'L' {
			max = (((min + max) as f32) / 2.0).floor() as u32;
		} else if c == 'B' || c == 'R' {
			min = (((min + max) as f32) / 2.0).ceil() as u32;
		} else {
			panic!("Unexpected c: {}", c);
		}
	}

	if min != max {
		panic!("Expected min={} to equal max={}", min, max);
	}

	return min;
}

fn get_seat_position(code: &str) -> (u32, u32) {
	let f7 = &code[0..7];
	let l3 = &code[7..];
	return ( resolve_binary(f7, 0, 127),  resolve_binary(l3, 0, 7) );
}

fn get_seat_id(code: &str) -> u32 {
	let (row, column) = get_seat_position(code);
	return row * 8 + column;
}

pub fn day5() {
	assert_eq!((70, 7), get_seat_position("BFFFBBFRRR"));
	assert_eq!((14, 7), get_seat_position("FFFBBBFRRR"));
	assert_eq!((102, 4), get_seat_position("BBFFBBFRLL"));

	let mut ids = HashSet::new();
	let mut current_max = 0;
	for r_line in read_lines("./data/day5.txt").unwrap() {
		let line = r_line.unwrap();
		let id = get_seat_id(&line);
		ids.insert(id);

		if id > current_max {
			current_max = id;
		}
	}

	println!("Max = {}", current_max);

	for id in 1..current_max {
		if !ids.contains(&id) {
			if ids.contains(&(id - 1)) && ids.contains(&(id + 1)) {
				println!("id={} is missing", id);
			}
		}
	}
}

