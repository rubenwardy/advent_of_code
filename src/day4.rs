use crate::utils::read_lines;
use regex::Regex;
use std::collections::HashMap;

const FIELDS: [&str; 8] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"];

fn is_field_valid(field: &str, value: &str) -> bool {
	fn check_number(value: &str, min: i32, max: i32) -> bool {
		return match value.parse::<i32>() {
			Ok(x) => x >= min && x <= max,
			Err(_) => false
		}
	}

	fn check_color(value: &str) -> bool {
		if value.chars().next().unwrap() != '#' {
			return false;
		}

		let hex = &value[1..];
		return hex.chars().count() == 6 &&
			hex.chars().all(|c| char::is_numeric(c) || (c >= 'a' && c <= 'z'));
	}

	fn check_height(value: &str, min: f32, max: f32) -> bool {
		let re_unit: Regex = Regex::new(r"^([0-9.]+)([a-z]+)$").unwrap();
		return match re_unit.captures(value) {
			None => false,
			Some(cap) => {
				let unit = &cap[2];
				if unit != "in" && unit != "cm" {
					return false;
				}

				let mult = if unit == "in" { 2.54 } else { 1.0 };
				let x = mult * cap[1].parse::<f32>().unwrap();
				x >= min && x <= max
			}
		};
	}

	return match field {
		"byr" => check_number(value, 1920, 2002),
		"iyr" => check_number(value, 2010, 2020),
		"eyr" => check_number(value, 2020, 2030),
		"hgt" => check_height(value, 150.0, 193.0),
		"hcl" => check_color(value),
		"ecl" => {
			return ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&&*value);
		},
		"pid" => {
			return value.chars().all(char::is_numeric) && value.chars().count() == 9;
		},
		&_ => panic!("Unexpected field {}", field)
	};
}

fn is_passport_valid(passport: &HashMap<String, String>, line_number: usize) -> bool {
	for (key, value) in passport.iter() {
		println!(" - {} = {}", key, value);
	}

	for field in FIELDS.iter() {
		if *field == "cid" {
			continue;
		}

		let o_value = passport.get(&field.to_string());
		match o_value {
			None => {
				println!("!!! Line {} is missing {}", line_number, &field);
				return false;
			}

			Some(value) => {
				if !is_field_valid(field, value) {
					println!("!!! Line {} has invalid value for {}: {}", line_number, &field, &value);
					return false;
				}
			}
		}

	}

	return true;
}

fn validate_passports() -> u32 {
	let re_field: Regex = Regex::new(r"([a-z]+):([^ \n]+)").unwrap();
	let mut count = 0;
	let mut passport: HashMap<String, String> = HashMap::new();

	for (line_number, r_line) in read_lines("./data/day4.txt").unwrap().enumerate() {
		let line = r_line.unwrap();

		if line == "" {
			if is_passport_valid(&passport, line_number) {
				count += 1;
			}
			passport.clear();
			println!("====")
		} else {
			println!("{}", line);
			for caps in re_field.captures_iter(&line) {
				passport.insert(caps.get(1).unwrap().as_str().to_string(),
								caps.get(2).unwrap().as_str().to_string());
			}
		}
	}

	if !passport.is_empty() {
		if is_passport_valid(&passport, 100000) {
			count += 1;
		}
	}

	println!("====");

	return count;
}

pub fn day4() {
	let count = validate_passports();
	println!("count: {}", count);
}
