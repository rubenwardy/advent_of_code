mod utils;
mod day3;
mod day1;
mod day2;
mod day4;
mod day5;
mod day7;
mod day8;

use crate::day8::day8;

fn main() {
	day8();
}
