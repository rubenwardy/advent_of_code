use crate::utils::read_lines;

fn get_numbers() -> Vec<i32> {
	let mut vector: Vec<i32> = vec![];
	let lines = read_lines("./data/day1.txt").unwrap();
	for line in lines {
		match line {
			Err(why)   => panic!("{:?}", why),
			Ok(string) => match string.trim().parse::<i32>() {
				Err(why)        => panic!(why),
				Ok(number)=> vector.push(number)
			}
		}
	}

	return vector;
}

pub fn day1() {
	let numbers = get_numbers();

	for i in 0..numbers.len() {
		for j in (i+1)..numbers.len() {
			let one = numbers[i];
			let two = numbers[j];
			if one + two == 2020 {
				println!("{}*{} = {}", one, two, one * two);
			}

			for k in (j+1)..numbers.len() {
				let three = numbers[k];
				if one + two + three == 2020 {
					println!("{}*{}*{} = {}", one, two, three, one * two * three);
				}
			}
		}
	}
}
