use crate::utils::read_lines;

type Map = Vec<Vec<char>>;

trait TwoD {
	fn get(&self, x: usize, y: usize) -> char;
}

impl TwoD for Map {
	fn get(&self, x: usize, y: usize) -> char {
		let row = &self[y];
		return row[x % row.len()];
	}
}

fn read_map() -> Map {
	let mut width = 0;
	let mut array: Map  = vec![vec![' '; 0]; 0];
	for line in read_lines("./data/day3.txt").unwrap() {
		let text = line.unwrap();

		let row : Vec<char> = text.trim().chars().collect();

		if width == 0 {
			width = row.len();
		} else if width != row.len() {
			panic!("row length is not equal to width");
		}

		array.push(row);
	}
	return array;
}

fn count_trees(map: &Map, sx: usize, sy: usize) -> usize {
	let mut count = 0;
	let mut x = 0;
	let mut y = 0;

	x += sx;
	y += sy;
	while y < map.len() {
		if map.get(x, y) == '#' {
			count += 1;
		}

		x += sx;
		y += sy;
	}

	return count;
}

pub fn day3() {
	let map = read_map();
	println!("{} trees", count_trees(&map, 3, 1));

	let xs = [ 1, 3, 5, 7, 1 ];
	let ys = [ 1, 1, 1, 1, 2 ];

	let product = xs.iter()
		.zip(ys.iter())
		.map(|(x,y)| count_trees(&map, *x, *y))
		.fold(1, |acc, x| acc * x);

	println!("product: {}", product);
}
