use regex::Regex;
use crate::utils::read_lines;

fn validate_password(password: &str, policy_char: char, min: usize, max: usize) -> bool {
	let counter = password.chars().filter(|c| *c == policy_char).count();
	return counter >= min && counter <= max ;
}

fn validate_password2(password: &str, policy_char: char, x: usize, y: usize) -> bool {
	let cx = password.chars().nth(x - 1).unwrap();
	let cy = password.chars().nth(y - 1).unwrap();

	return (cx == policy_char) ^ (cy == policy_char);
}


pub fn day2() {
	let re : Regex = Regex::new(r"^(\d+)-(\d+) ([a-z]+): (.+)$").unwrap();

	let mut valid = 0;

	for line in read_lines("./data/day2.txt").unwrap() {
		let text = line.unwrap();

		match re.captures(&text) {
			Some(cap) => {
				let policy_char : char = cap[3].chars().next().unwrap();
				let min = cap[1].parse::<usize>().unwrap();
				let max = cap[2].parse::<usize>().unwrap();
				if validate_password2(&cap[4], policy_char, min, max) {
					valid += 1;
				}
			},
			None => println!("Failed to match on {}", text)
		}
	}

	println!("{} are valid", valid);
}

