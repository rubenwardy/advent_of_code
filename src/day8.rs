use crate::utils::read_lines;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fmt;
use std::ops::Index;

#[derive(PartialEq)]
enum Op {
	Nop,
	Acc,
	Jmp
}

impl fmt::Display for Op {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let value = match self {
			Op::Nop => "nop",
			Op::Acc => "acc",
			Op::Jmp => "jmp"
		};

		write!(f, "{}", value)
	}
}

struct Memory {
	acc: i32,
	pc: usize
}

struct Instruction {
	op: Op,
	imm: i32
}

impl Instruction {
	fn apply(&self, memory: &mut Memory) {
		match self.op {
			Op::Nop => memory.pc += 1,
			Op::Acc => {
				memory.acc += self.imm;
				memory.pc += 1;
			},
			Op::Jmp => {
				memory.pc = (memory.pc as i32 + self.imm) as usize;
			}
		}
	}

	fn flip(&self) -> Instruction {
		let alt_op = if self.op == Op::Nop { Op::Jmp } else { Op::Nop };
		return Instruction { op: alt_op, imm: self.imm };
	}
}

trait Program {
	fn get(&self, pc: usize) -> &Instruction;
	fn len(&self) -> usize;
}

impl Program for Vec<Instruction> {
	fn get(&self, pc: usize) -> &Instruction {
		return &self[pc];
	}

	fn len(&self) -> usize {
		return self.len();
	}
}

struct PatchedProgram<'a> {
	program: &'a dyn Program,
	u_pc: usize,
	instruction: Instruction
}

impl Program for PatchedProgram<'_> {
	fn get(&self, pc: usize) -> &Instruction {
		return if pc == self.u_pc { &self.instruction } else { self.program.get(pc) };
	}

	fn len(&self) -> usize {
		return self.program.len();
	}
}

fn parse_program() -> Vec<Instruction> {
	let re_rule: Regex = Regex::new(r"^([a-z]+) ([+-]?\d+)$").unwrap();

	let mut ret = vec![];
	for r_line in read_lines("./data/day8.txt").unwrap() {
		let line = r_line.unwrap();
		let caps = re_rule.captures(&line).unwrap();
		let op_name = caps.get(1).unwrap().as_str();
		let imm = caps.get(2).unwrap().as_str();

		let op = match op_name {
			"nop" => Op::Nop,
			"acc" => Op::Acc,
			"jmp" => Op::Jmp,
			_ => panic!("Unknown op name: {}", op_name)
		};

		let imm = imm.parse::<i32>().unwrap();
		ret.push(Instruction { op, imm });
	}

	return ret;
}

struct VM<'a> {
	program: &'a dyn Program,
	memory: Memory,
	executed_instructions: Vec<bool>
}

impl VM<'_> {
	fn new(program: &dyn Program) -> VM {
		return VM {
			program,
			memory:  Memory { acc: 0 , pc: 0 },
			executed_instructions: vec![false; program.len()]
		};
	}

	/// Execute current PC, returning true if there's more to execute
	fn step(&mut self) -> bool {
		if self.memory.pc >= self.program.len() {
			return false;
		}

		self.executed_instructions[self.memory.pc] = true;

		let inst = &self.program.get(self.memory.pc);
		println!("pc={}: {} {}  |  acc={}", self.memory.pc, inst.op, inst.imm, self.memory.acc);
		inst.apply(&mut self.memory);
		return true;
	}

	fn step_until_repeat(&mut self) -> bool {
		return !self.is_repeated() && self.step();
	}

	fn is_infinite_loop(&mut self) -> bool {
		while self.step_until_repeat() {}
		!self.is_finished()
	}

	/// Whether the VM has finished executing
	fn is_finished(&self) -> bool {
		return self.memory.pc >= self.program.len();
	}

	/// Whether the current PC has ran before
	fn is_repeated(&self) -> bool {
		return !self.is_finished() && self.executed_instructions[self.memory.pc];
	}

	/// Get next `Instruction`
	fn get_inst(&self) -> &Instruction {
		return &self.program.get(self.memory.pc);
	}

	/// Get next PC
	fn get_pc(&self) -> usize {
		return self.memory.pc;
	}

	/// Get accumulator
	fn get_acc(&self) -> i32 {
		return self.memory.acc;
	}

	/// Copy VM and change the underlying program
	fn clone_update_program<'m>(&self, program: &'m dyn Program) -> VM<'m> {
		return VM {
			program,
			memory:  Memory { acc: self.memory.acc , pc: self.memory.pc },
			executed_instructions: self.executed_instructions.clone()
		};
	}
}

fn run_program(program: &Vec<Instruction>) {
	let mut vm = VM::new(program);
	while !vm.is_repeated() && vm.step() {}
}

fn solve_program(program: &Vec<Instruction>) {
	let mut vm = VM::new(program);
	while { // do-while
		let inst = vm.get_inst();
		if (inst.op == Op::Nop && inst.imm != 0) || inst.op == Op::Jmp {
			let patch = PatchedProgram { program, u_pc: vm.get_pc(), instruction: inst.flip() };
			let mut lookahead = vm.clone_update_program(&patch);
			if !lookahead.is_infinite_loop() {
				println!("Found solution - pc={}.  Final acc={}", lookahead.get_pc(), lookahead.get_acc());
				return;
			}
		}

		vm.step_until_repeat()
	} {}
}

pub fn day8() {
	let instructions = parse_program();
	solve_program(&instructions);
}

