use crate::utils::read_lines;
use regex::Regex;
use std::collections::{HashMap, HashSet};

struct TreeNode {
	color: String,
	children: Vec<(u32, String)>
}

type Tree = HashMap<String, TreeNode>;

/// Produce a `Tree` from a file.
fn parse_tree() -> Tree {
	let re_rule: Regex = Regex::new(r"^(\w+ \w+) bags? contain (.+).$").unwrap();
	let re_inner: Regex = Regex::new(r"(\d+) (\w+ \w+) bags?").unwrap();

	let mut ret = HashMap::new();

	for r_line in read_lines("./data/day7.txt").unwrap() {
		let line = r_line.unwrap();

		let caps = re_rule.captures(&line).unwrap();
		let color = caps.get(1).unwrap().as_str();
		let inner = caps.get(2).unwrap().as_str();

		let children =
			re_inner.captures_iter(&inner)
				.map(|x| (x[1].parse::<u32>().unwrap(), x[2].to_string()))
				.collect();

		ret.insert(color.to_string(), TreeNode { color: color.to_string(), children });
	}

	return ret;
}


/// From a `Tree`, produce a new `Tree` with an inverted tree structure,
/// such that `children` is actually the parents.
fn build_inverse(tree: &Tree) -> Tree {
	let mut inversed_tree: Tree = HashMap::new();
	for node in tree.values() {
		inversed_tree.entry(node.color.to_string())
			.or_insert_with(|| TreeNode { color: node.color.to_string(), children: vec![] });

		for (_count, child_color) in node.children.iter() {
			let other_node =
				inversed_tree.entry(child_color.to_string())
					.or_insert_with(|| TreeNode { color: child_color.to_string(), children: vec![] });

			other_node.children.push((1, node.color.to_string()));
		}
	}

	return inversed_tree;
}


/// For a particular color, count how many unique ancestors it has of any separation
fn count_all_unique_ancestors(tree: &Tree, color: &str) -> usize {
	fn visit(inverse: &Tree, visited: &mut HashSet<String>, node: &TreeNode)  {
		if visited.contains(&node.color) {
			return;
		}

		visited.insert(node.color.to_string());

		for (_count, child_color) in node.children.iter() {
			let child = inverse.get(child_color).unwrap();
			visit(inverse, visited, child);
		}
	}

	let inverse = build_inverse(tree);
	let mut visited = HashSet::new();
	visit(&inverse, &mut visited, inverse.get(color).unwrap());
	return visited.len() - 1;
}


fn count_all_descendants(tree: &Tree, color: &str) -> u32 {
	let node = tree.get(color).unwrap();

	let mut ret = 0;
	for (count, child_color) in node.children.iter() {
		ret += count * (1 + count_all_descendants(tree, child_color));
	}

	return ret;
}


pub fn day7() {
	let tree = parse_tree();

	let ancestors = count_all_unique_ancestors(&tree, "shiny gold");
	println!("ancestors = {}", ancestors);

	let descendants = count_all_descendants(&tree, "shiny gold");
	println!("descendants = {}", descendants);
}

