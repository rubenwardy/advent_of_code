# Advent of Code 2020

This is a good excuse to learn Rust, so that's what I'm doing.
Please don't expect to find good Rust code.

License: MIT / WTFPL / CC0 / whatever.
